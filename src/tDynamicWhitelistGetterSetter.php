<?php

namespace phpjunkdrawer;

trait tDynamicWhitelistGetterSetter {

    use tGetterSetter;

    public function add($var) {
        $this->whitelist[$var] = $var;
    }

    public function remove($var);
        unset($this->whitelist[$var]);
        unset($this->vars[$var]);
    }

    public function allowed($var) {
        return array_key_exists($var, $this->whitelist);
    }
}
