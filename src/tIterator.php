<?php

namespace phpjunkdrawer;

trait tIterator {

    private $array = array();
    private $key = 0;

    // Iterator methods
    public function rewind() {
        reset($this->array);
        $this->key = 0;
    }

    public function current() {
        return $this->array[$this->key];
    }

    public function key() {
        return $this->key;
    }

    public function next() {
        ++$this->key;
    }

    public function valid() {
        return isset($this->array[$this->key]);
    }
}
