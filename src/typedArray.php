<?php

namespace phpjunkdrawer;

/**
 * An array-emulating class that only accepts values of a certain type
 */
class typedArray implements ArrayAccess {

    use tIterator;

    private $type;

    public function __construct($type) {
        $this->type = $type;
    }

    public function offsetSet($offset, $value) {
        if ( ( $type = gettype($value) ) == 'object' ) {
            $type = get_class($value);
        }
        if ( $this->type != $type ) {
            throw new \InvalidArgumentException('Type \'' . $type . '\' is not allowed.');
        }

        if (is_null($offset)) {
            $this->array[] = $value;
        } else {
            $this->array[$offset] = $value;
        }
    }

    public function offsetExists($offset) {
        return isset($this->array[$offset]);
    }

    public function offsetUnset($offset) {
        unset($this->array[$offset]);
    }

    public function offsetGet($offset) {
        return isset($this->array[$offset]) ? $this->array[$offset] : null;
    }
}
