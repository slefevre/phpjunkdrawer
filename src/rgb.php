<?php

namespace phpjunkdrawer;

/**
 * A class that accepts 0-255 values for RGB colors
 */
class rgb {

    private $r;
    private $g;
    private $b;

    public function __construct($r,$g,$b) {
        $this->r($r);
        $this->g($g);
        $this->b($b);
    }

    public static function hex($hex) {
        $hex = str_replace("#", "", $hex);
        if(strlen($hex) == 3) {
            $r = hexdec($hex[0].$hex[0]);
            $g = hexdec($hex[1].$hex[1]);
            $b = hexdec($hex[2].$hex[2]);
        } else {
            $r = hexdec($hex[0].$hex[1]);
            $g = hexdec($hex[2].$hex[3]);
            $b = hexdec($hex[4].$hex[5]);
        }
        return new rgbColor($r, $g, $b);
    }

    public function r($value = NULL) {
        return $this->color(__FUNCTION__, $value);
    }
    public function g($value = NULL) {
        return $this->color(__FUNCTION__, $value);
    }
    public function b($value = NULL) {
        return $this->color(__FUNCTION__, $value);
    }

    private function color($method, $value = NULL) {
        if ( ! is_null($value) ) {
            $this->validate($method, $value);
            $this->$method = $value;
        }
        return $this->$method;
    }

    private function validate($color, $value) {
        if ( ! is_int($value) ) {
            throw new \InvalidArgumentException("The color value specified for $color, $value, is not an integer.");
        }
        if ( $value > 255 || $value < 0 ) {
            throw new \InvalidArgumentException("The color value specified for $color, $value, is outside the valid range of 0 - 255.");
        }
    }

    public function getRgb() {
        return array(
              'r' => $this->r
            , 'g' => $this->g
            , 'b' => $this->b
        );
    }

    public function getHex() {
        return '#' . sprintf('%02x', $this->r) . sprintf('%02x', $this->g) . sprintf('%02x', $this->b);
    }

    public function __toString() {
        return $this->getHex();
    }
}
