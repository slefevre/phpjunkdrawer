<?php

namespace phpjunkdrawer;

/**
 * A trait that handles getting and setting of properties
 *
 * trait tGetterSetter
 */
trait tGetterSetter {

    private $vars = array(); // store values here

    /**
     * @return void
     */
    public function __set($var, $val) {
        $this->vars[$var] = $val;
    }

    /**
     * @return void
     */
    public function __get($var) {
        return $this->vars[$var];
    }

    /**
     * @return result of isset()
     */
    public function __isset($var) {
        return isset($this->vars[$var]);
    }

    /**
     * @return void
     */
    public function __unset($var) {
        unset($this->vars[$var]);
    }
}
