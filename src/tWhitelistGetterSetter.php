<?php

namespace phpjunkdrawer;

trait tWhitelistGetterSetter {

    private $vars = array();
    private $whitelist;

    public function __construct(array $whitelist) {
        // make the whitelist keys (names of the allowed variables)
        // the same as the values, for easier array searching
        $this->whitelist = array_combine($whitelist, $whitelist);
    }

    private function validate($var) {
        if ( ! array_key_exists($var, $this->whitelist) ) {
            throw new UnexpectedValueException('The var \'$var\' is not allowed in this instance.');
        }
    }

    public function __set($var, $val) {
        $this->validate($var);
        $this->vars[$var] = $val;
    }

    public function __get($var) {
        $this->validate($var);
        return $this->vars[$var];
    }

    public function __isset($var) {
        $this->validate($var);
        return isset($this->vars[$var]);
    }

    public function __unset($var) {
        $this->validate($var);
        unset($this->vars[$var]);
    }
}
